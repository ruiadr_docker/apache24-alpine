
FROM httpd:2.4-alpine

RUN apk update

RUN mkdir /usr/local/apache2/conf.d

RUN echo 'IncludeOptional conf.d/*.conf' >> /usr/local/apache2/conf/httpd.conf

RUN sed -i '/LoadModule expires_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule deflate_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule headers_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule proxy_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule proxy_fcgi_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule rewrite_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule remoteip_module/s/^#//g' /usr/local/apache2/conf/httpd.conf

RUN sed -i '/DirectoryIndex index.html/s/index\.html/index\.php index\.html/g' /usr/local/apache2/conf/httpd.conf

RUN echo -e "\n<IfModule remoteip_module>\n    RemoteIPHeader X-Real-IP\n</IfModule>" >> /usr/local/apache2/conf/httpd.conf

EXPOSE 80
